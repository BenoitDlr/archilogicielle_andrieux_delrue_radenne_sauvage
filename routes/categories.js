const EntityManager = require('../models/EntityManager.js')
var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')

router.use(bodyParser.urlencoded({ extended: true }))

/* GET index stock page */
router.get('/', function (req, res, next) {
  res.render('categories', { title: 'Gestion des catégories', succes: '' })
})

/* GET new product page */
router.get('/createCategorie', function (req, res, next) {
  res.render('createCategorie', { title: 'Nouvelle catégorie', succes: '' })
})

/* POST new product page */
router.post('/createCategorie', function (req, res, next) {
  console.log(req.body)
  var manager = new EntityManager()
  manager.persistenceCategorie(req.body)
  // var manager = new EntityManager()
  // EntityManager.persistence(req.body)
  // var listeCategorie = ['Apple', 'Banana', 'Chibre']

  res.render('createCategorie', { title: 'Nouvelle catégorie', succes: 'Votre catégorie a été enregistrée' })
})

module.exports = router
