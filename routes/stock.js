const EntityManager = require('../models/EntityManager.js')
var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')

router.use(bodyParser.urlencoded({ extended: true }))

/* GET index stock page */
router.get('/view', function (req, res, next) {
  res.render('stock', { title: 'Gestion du stock' })
})

/* GET list stock page */
router.get('/list', function (req, res, next) {
  var manager = new EntityManager()

  manager.getProductList(function (listeProduit) {
    console.log(listeProduit)
    if (listeProduit === undefined) {
      listeProduit = [{ id: 1, nom: '', categorie: '', prix: '', stock: '' }]
    }
    res.render('listProduct', { title: 'Liste des produits', listeProduit: listeProduit })
  })
})

router.get('/suppress/:id', function (req, res, next) {
  var manager = new EntityManager()
  manager.deleteProduit(req.params.id)
  manager.getProductList(function (listeProduit) {
    console.log(listeProduit)
    if (listeProduit === undefined) {
      listeProduit = [{ id: 1, nom: '', categorie: '', prix: '', stock: '' }]
    }
    res.render('listProduct', { title: 'Liste des produits', listeProduit: listeProduit })
  })
})

/* GET new product page */
router.get('/createProduct', function (req, res, next) {
  var manager = new EntityManager()
  manager.getCategories(function (listeCategorieBack) {
    console.log(listeCategorieBack)
    res.render('newProduct', { title: 'Nouveau produit', listeCategorie: listeCategorieBack, succes: '' })
  })
})

/* POST new product page */
router.post('/createProduct', function (req, res, next) {
  var manager = new EntityManager()
  manager.persistenceProduit(req.body)
  manager.getCategories(function (listeCategorieBack) {
    console.log(listeCategorieBack)
    res.render('newProduct', { title: 'Nouveau produit', listeCategorie: listeCategorieBack, succes: 'Votre produit a été enregistré' })
  })
})

router.get('/update/:id/:nom/:cat/:prix/:stock', function (req, res, next) {
  var stock = req.params.stock.substring(req.params.stock.indexOf(' ') + 1, req.params.stock.indexOf(':'))
  var qte = req.params.stock.substring(req.params.stock.indexOf(':') + 1)
  var product = {
    id: req.params.id,
    nom: req.params.nom,
    categorie: req.params.cat,
    prix: req.params.prix,
    idStock: stock,
    quantite: qte
  }
  res.render('updateProduct', { title: 'Modifier un produit', product: product })
})

module.exports = router
