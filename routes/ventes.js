const EntityManager = require('../models/EntityManager.js')
var express = require('express')
var router = express.Router()

/* GET index vente page. */
router.get('/list', function (req, res, next) {
  var manager = new EntityManager()

  manager.getVenteList(function (listeVente) {
    console.log(listeVente)
    if (manager.getVenteList === undefined) {
      listeVente = [{ id: 1, nom: '', categorie: '', quantite: '', dateVente: '', prix: '' }]
    }
    res.render('ventes', { title: 'Consultation des ventes', listeVente: listeVente })
  })
})

/* GET index vente page. */
router.get('/createVente', function (req, res, next) {
  res.render('newVente', { title: 'Nouvelle vente', succes: '' })
})

router.get('/suppress/:id', function (req, res, next) {
  var manager = new EntityManager()
  manager.deleteVente(req.params.id)
  manager.getVenteList(function (listeVente) {
    console.log(listeVente)
    if (manager.getVenteList === undefined) {
      listeVente = [{ id: 1, nom: '', categorie: '', quantite: '', dateVente: '', prix: '' }]
    }
    res.render('ventes', { title: 'Consultation des ventes', listeVente: listeVente })
  })
})

/* GET index vente page. */
router.post('/createVente', function (req, res, next) {
  var manager = new EntityManager()
  manager.persistenceVente(req.body)
  res.render('newVente', { title: 'Nouvelle vente', succes: 'Votre vente a été enregistrée' })
})
module.exports = router
