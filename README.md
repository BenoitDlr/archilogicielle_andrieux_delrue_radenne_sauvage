ArchiLogicielle_ANDRIEUX_DELRUE_RADENNE_SAUVAGE

Plateforme de gestion des stocks d'un magasin

Configuration pratique pour réaliser convenablement le projet :
- Installer Docker (Docker Toolbox si vous ne possédez pas la version pro de Windows sinon Docker Engine)
- Editeur : VSCode avec les packages suivants --> SQLServer (mssql), Docker, Babel Javascript, ESLint

Installer Docker peut être un peu tricky. Assurez-vous avant d'avoir la virtualisation active dans le BIOS de votre ordinateur. Attention également à bien décocher l'installation de VirtualBox si vous en avez déjà une. Une fois la toolbox installée, il faut lancer Docker quickstart terminal qui s'occupera de l'installation comme un grand. Mais attention, il se peut que même avec la virtualisation active dans le BIOS le driver ne soit pas reconnu et que l'installation de VirtualBox ne s'effectue pas correctement. Pour modifier ça, il faut retrouver le dossier DockerToolbox dans le folder des Programmes et modifier la ligne 69 par cette instruction :
"${DOCKER_MACHINE}" create -d virtualbox --virtualbox-no-vtx-check $PROXY_ENV "${VM}"

Dernièrement, il se peut que Docker ait du mal avec la propre version de VirtualNox qu'il installe. Ce que Docker suggère et qui a marché chez moi c'est de désinstaller et réinstaller manuellement VirtualBox en prenant une des dernières versions à disposition.

---------------------------------------------------------

Notes relatives au projet (Mises à jour 27/01/2020) :
- [Auteur: Benito, Sujet: Architecture] : Nous devons réaliser un schéma d'architecture pour l'application, schéma que nous devrons du coup appliquer au développement. La personne qui souhaite prendre en charge la conception de l'architecture est la bienvenue pour que l'on puisse partir sur des bases saines demain (tout en appliquant ce qu'on s'était dit à savoir du micro-service si mes souvenirs sont bons).
- [Auteur: Benito, Sujet: Todo] : Ce soir j'essaye d'établir une base de données respectant le schéma du prof et de mettre en place le routing minimal avec une page d'accueil, en attendant de se répartir convenablement les tâches demain.
- 

---------------------------------------------------------

Installation du projet :
1. -Créer un SGBD SQL (à l'aide de wamp ou de sqlworkbench)
2. -Créer une base de donnée "nodedb"
3. -Renseigner les informations de connexion dans le fichier EntityManager.js (/models/EntityManager.js) :
     il faut respectivement renseigner les informations au lignes suivantes :

![Image](https://i.imgur.com/g44ljBz.png)

Respectivement : - adresse du SGBD
                 - nom de la base
                 - nom d'utilisateur
                 - mot de passe

       
(Ces informations étaient précédement stockée dans baseLogs.txt mais il peut rencontrer un problème à la lecture en fonction de la configuration du poste)
       
4. -Lancer le script 'Script création de base' présent à la racine du projet sur la base
5. -Lancer le projet à l'aide du Node.js command prompt : node app.js

Projet réalisé par :
Pierre ANDRIEUX
Benoit DELRUE
Maximilien SAUVAGE
Cassandre RADENNE
