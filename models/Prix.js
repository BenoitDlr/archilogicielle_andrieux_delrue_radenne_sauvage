const dataModelObject = require('./dataModelObject.js')

module.exports = class Prix extends dataModelObject {
  // Constructeur
  constructor (valeur, idProduit, debut, fin) {
    super()
    if (idProduit === undefined) throw new Error('Pas d\'id Produit !')
    if (debut === undefined) debut = JSDateToMYSQLDate(new Date()) // Date du jour par défaut
    if (fin === undefined) fin = JSDateToMYSQLDate(new Date('January 1, 2200 00:00:00')) // Pas de date de fin par défaut
    if (isNaN(valeur) || valeur === undefined) valeur = 0 // Valeur nulle comme valeur par défaut.
    // Setup des data
    this.idProduit = idProduit
    this.dateDebut = debut
    this.dateFin = fin
    this.valeur = valeur
  }

  persist (db) {
    // Requete d'insertion
    var insertionPrix = 'INSERT INTO Prix(idProduit,dateDebut,dateFin,Valeur) VALUES (' + this.getIdProduit() + ',\'' + this.getDateDebut() + '\',\'' + this.getDateFin() + '\',' + this.getValeur() + ')'
    db.query(insertionPrix, function (err) {
      if (err) throw err
      else {
        console.log('1 row added to Prix')
      }
    })
  }

  // Getter & Setters
  getId () {
    return this.id
  }

  getIdProduit () {
    return this.idProduit
  }

  getDateDebut () {
    return this.dateDebut
  }

  getDateFin () {
    return this.dateFin
  }

  getValeur () {
    return this.valeur
  }

  setId (id) {
    this.id = id
  }

  setIdProduit (idProd) {
    this.idProduit = idProd
  }

  setDateDebut (debut) {
    this.dateDebut = debut
  }

  setDateFin (fin) {
    this.dateFin = fin
  }

  setValeur (v) {
    this.valeur = v
  }
}

function JSDateToMYSQLDate (dateJs) {
  var SQLDate = dateJs.toISOString().split('T')[0] + ' ' + dateJs.toTimeString().split(' ')[0]
  return SQLDate
}
