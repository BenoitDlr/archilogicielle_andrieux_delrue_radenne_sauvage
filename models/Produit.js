const dataModelObject = require('./dataModelObject.js')

module.exports = class Produit extends dataModelObject {
  // Constructeur
  constructor (nom, idCategorie) {
    super()
    if (nom === undefined) throw new Error('Unamed product created') // Date du jour par défaut
    if (idCategorie === undefined) idCategorie = 0 // Categorie 0 étant la categorie "Inconnue"
    // Setup de la donnée
    this.nom = nom
    this.idCategorie = idCategorie
  }

  persist (db) {
    // Requête d'ajout du Produit
    var insertionProduit = 'INSERT INTO Produit(nom,idCategorie) VALUES (\'' + this.getNom() + '\',' + this.getIdCategorie() + ')'
    db.query(insertionProduit, function (err) {
      if (err) throw err
      else console.log('1 row inserted in Produit')
    })
  }

  // Getter & Setters
  getId () {
    return this.id
  }

  getNom () {
    return this.nom
  }

  getIdCategorie () {
    return this.idCategorie
  }

  setId (id) {
    this.id = id
  }

  setNom (nom) {
    this.nom = nom
  }

  setIdCategorie (idCategorie) {
    this.idCategorie = idCategorie
  }
}
