const dataModelObject = require('./dataModelObject.js')

module.exports = class Categorie extends dataModelObject {
  // Constructeur
  constructor (label, description) {
    super()
    if (label === undefined) throw new Error('Name missing in Categorie') // Le nom de categorie est obligatoire.
    if (description === undefined) description = '-'
    this.label = label
    this.description = description
  }

  persist (db) {
    // Requête d'ajout de la Categorie
    var insertionCategorie = 'INSERT INTO Categorie(label,description) VALUES (\'' + this.getLabel() + '\',\'' + this.getDescr() + '\')'
    db.query(insertionCategorie, function (err) {
      if (err) throw err
      else console.log('1 row inserted in Categorie')
    })
  }

  // Getter & Setters
  getIdCat () {
    return this.idCat
  }

  getLabel () {
    return this.label
  }

  getDescr () {
    return this.description
  }

  setIdCat (idCat) {
    this.idCat = idCat
  }

  setLabel (label) {
    this.label = label
  }

  setDescr (description) {
    this.description = description
  }
}
