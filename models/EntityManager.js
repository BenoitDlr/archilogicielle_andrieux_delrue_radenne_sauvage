const Produit = require('./Produit.js')
const Prix = require('./Prix.js')
const StocMv = require('./StocMv.js')
const ProduitVente = require('./ProduitVente.js')
const Vente = require('./Vente.js')
const Categorie = require('./Categorie.js')

var mysql = require('mysql')
// var fs = require('fs')

module.exports = class EntityManager {
  constructor () {
    // Recuperation des logins de base
    // var loginDB = fs.readFileSync('./baseLogs.txt', 'utf8')
    // var infoDb = loginDB.split('\r\n')
    // Assignation des infos
    this.host = 'localhost'
    this.dbName = 'node'
    this.user = 'root'
    this.password = 'password'
    // Creation de la connexion à la base
    this.db = mysql.createConnection({
      host: this.host,
      user: this.user,
      password: this.password,
      database: this.dbName
    })
    this.db.connect(function (err) {
      if (err) {
        console.log('Error DB Connection')
      } else {
        console.log('Connected to ' + this.dbName)
      }
    })
  }

  persistenceCategorie (businessObject) {
    // Recuperation des données de la nouvelle Categorie
    var catName = businessObject.nom
    var catDescr = businessObject.description
    console.log(catName)
    console.log(catDescr)
    // Objet DAO
    var newCat = new Categorie(catName, catDescr)
    newCat.persist(this.db)
  }

  persistenceProduit (businessObject) {
    var productName = businessObject.nom // Nom du nouveau produit
    var categorie = businessObject.categorie // Format : 'id-Label'
    var cat = categorie.split('-')// Recuperation de l'id de la categorie
    var valeurPrix = businessObject.prix // Valeur du prix
    var idStock = businessObject.idStock // Dans quel stock est placé le nouveau produit
    var stockQuantite = businessObject.quantite // Combien de produit il y a dans le stock donné.
    // Requête d'ajout du Prix du produit
    var prod = new Produit(productName, cat[0])
    // Persistence des objets crée.
    prod.persist(this.db)
    // console.log(valeurPrix)
    // console.log(productName)
    // console.log(idStock)
    // console.log(stockQuantite)
    this.persistencePrix(this.db, valeurPrix, productName)
    this.persistenceStock(this.db, idStock, stockQuantite, productName)
  }

  persistencePrix (db, valeur, productName) {
    // Requête des catégories dans la base
    var querySelectLastProd = 'SELECT idProduit FROM produit WHERE nom = \'' + productName + '\''
    this.db.query(querySelectLastProd, function (err, rows) {
      if (err) throw err
      var price
      rows.forEach((row) => {
        price = new Prix(valeur, row.idProduit)
      })
      price.persist(db)
    })
  }

  persistenceStock (db, idStock, quantite, productName) {
    // Requête des catégories dans la base
    var querySelectLastProd = 'SELECT idProduit FROM produit WHERE nom = \'' + productName + '\''
    this.db.query(querySelectLastProd, function (err, rows) {
      if (err) throw err
      var stock
      rows.forEach((row) => {
        stock = new StocMv(idStock, row.idProduit, quantite)
      })
      stock.persist(db)
    })
  }

  persistenceVente (businnessObject) {
    var idProd = businnessObject.idProd
    var qtProd = businnessObject.quantite
    var prixTotal = businnessObject.prix

    var vente = new Vente(prixTotal)
    vente.persist(this.db)

    this.persistencePV(this.db, idProd, qtProd, prixTotal, vente.getDate())
  }

  persistencePV (db, idProduit, quantite, prix, date) {
    var querySelectLastVente = 'SELECT idVente FROM Vente WHERE dateVente = \'' + date + '\' AND prixTotal = ' + prix
    console.log(querySelectLastVente)
    this.db.query(querySelectLastVente, function (err, rows) {
      if (err) throw err
      var pv
      rows.forEach((row) => {
        pv = new ProduitVente(idProduit, row.idVente, quantite)
      })
      pv.persist(db)
    })
  }

  getCategories (callback) {
    var allCat = []
    // Requête des catégories dans la base
    this.db.query('SELECT * FROM categorie', function (err, rows) {
      if (err) throw err
      rows.forEach((row) => {
        allCat.push(row.idCat + '-' + row.label) // 1-nom
      })
      callback(allCat) // Envoie dans un callback dans le main
    })
  }

  getProductList (callback) {
    var allProduct = []
    var req = 'SELECT Produit.idProduit, Produit.nom, Categorie.label, Prix.Valeur, stoc_mv.idStock, stoc_mv.quantite FROM ((Categorie JOIN Produit ON Produit.idCategorie = Categorie.idCat) JOIN Prix ON Prix.idProduit = Produit.idProduit) JOIN stoc_mv ON stoc_mv.idProduit = Produit.idProduit'
    this.db.query(req, function (err, rows) {
      if (err) throw err
      rows.forEach((row) => {
        var stockItem = 'stock ' + row.idStock + ': ' + row.quantite
        allProduct.push({ id: row.idProduit, nom: row.nom, categorie: row.label, prix: row.Valeur, stock: stockItem })
      })
      callback(allProduct) // Envoie dans un callback dans le main
    })
  }

  getVenteList (callback) {
    var allVente = []
    var req = 'SELECT Vente.idVente, Produit.nom, ProduitVente.quantite, Vente.DateVente, Vente.prixTotal FROM (Produit JOIN ProduitVente ON Produit.idProduit = ProduitVente.idProduit)JOIN Vente ON ProduitVente.idVente = Vente.idVente'
    this.db.query(req, function (err, rows) {
      if (err) throw err
      rows.forEach((row) => {
        console.log(row)
        allVente.push({ id: row.idVente, nom: row.nom, quantite: row.quantite, dateVente: row.DateVente, prix: row.prixTotal })
      })
      callback(allVente) // Envoie dans un callback dans le main
    })
  }

  deleteProduit (id) {
    var req = 'DELETE FROM Produit WHERE idProduit = ' + id
    this.db.query(req, function (err) {
      if (err) throw err
      else console.log('Product Dropped')
    })
    this.deletePrixCasc(id)
  }

  deletePrixCasc (id) {
    var req = 'DELETE FROM Prix WHERE idProduit = ' + id
    this.db.query(req, function (err) {
      if (err) throw err
      else console.log('Prix Dropped')
    })
  }

  deleteCategories (id) {
    var req = 'DELETE FROM Categorie WHERE idCat = ' + id
    this.db.query(req, function (err) {
      if (err) throw err
      else console.log('Categorie Dropped')
    })
  }

  deleteVente (id) {
    var req = 'DELETE FROM Vente WHERE idVente = ' + id
    this.db.query(req, function (err) {
      if (err) throw err
      else console.log('Vente Dropped')
    })
    this.deleteProduitVenteCasc(id)
  }

  deleteProduitVenteCasc (id) {
    var req = 'DELETE FROM ProduitVente WHERE idVente = ' + id
    this.db.query(req, function (err) {
      if (err) throw err
      else console.log('Produit Vente Dropped')
    })
  }

  updateProduitBusiness (id, change) {
    var productName = change.nom
    var idCat = change.categorie
    var valeurPrix = change.prix
    var idStock = change.idStock
    var stockQuantite = change.quantite

    // Requête d'update produit
    var req = 'UPDATE Produit SET nom = \'' + name + '\'' + ',idCategorie = ' + idCat + ' WHERE idProduit = ' + id
    this.db.query(req, function (err) {
      if (err) throw err
      else console.log('Product updated')
    })
    // Requête d'update du Stock
    var req2 = 'UPDATE stoc_mv SET dateInventaire = \'' + JSDateToMYSQLDate(new Date()) + '\'' + ',quantite = ' + stockQuantite + ' WHERE idProduit = ' + id + ' AND idStock = ' + idStock
    this.db.query(req2, function (err) {
      if (err) throw err
      else console.log('Stock ' + idStock + ' updated')
    })
    // Requete Update prix
    var req3 = 'UPDATE Prix SET dateFin = \'' + JSDateToMYSQLDate(new Date()) + '\' WHERE idProduit = ' + id + ' AND Valeur =' + valeurPrix
    this.db.query(req3, function (err) {
      if (err) throw err
      else console.log('Prix de ' + productName + 'updated')
    })
    this.persistencePrix(this.db, valeurPrix, productName)
  }

  updateCategorie (id, change) {
    var catName = change.nom
    var catDescr = change.description
    var req = 'UPDATE Categorie SET label = \'' + catName + '\'' + ',description = ' + catDescr + ' WHERE idCat = ' + id
    this.db.query(req, function (err) {
      if (err) throw err
      else console.log('Categorie ' + catName + 'updated')
    })
  }

  updateVenteBusiness (id, change) {
    var idProd = change.idProd
    var qtProd = change.quantite
    var prixTotal = change.prix

    var req = 'UPDATE Vente SET prixTotal = \'' + prixTotal + '\'' + ' WHERE idVente = ' + id
    this.db.query(req, function (err) {
      if (err) throw err
      else console.log('Vente updated')
    })
    //
    var req2 = 'UPDATE ProduitVente SET quantite = ' + qtProd + ' WHERE idVente = ' + id + ' AND idProduit = ' + idProd
    this.db.query(req2, function (err) {
      if (err) throw err
      else console.log('ProduitVente Updated')
    })
  }
}

function JSDateToMYSQLDate (dateJs) {
  var SQLDate = dateJs.toISOString().split('T')[0] + ' ' + dateJs.toTimeString().split(' ')[0]
  return SQLDate
}
