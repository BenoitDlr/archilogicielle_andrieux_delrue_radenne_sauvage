const dataModelObject = require('./dataModelObject.js')

module.exports = class StocMv extends dataModelObject {
  // Constructeur
  constructor (idStock, idProduit, quantite, date) {
    super()
    if (idStock === undefined || idProduit === undefined) throw new Error('ID missing in Stock') // Pas d'id, pas de construction
    if (isNaN(quantite) || quantite < 0) quantite = 0
    if (date === undefined) date = JSDateToMYSQLDate(new Date())

    this.idStock = idStock
    this.idProduit = idProduit
    this.date = date
    this.quantite = quantite
  }

  persist (db) {
    // Requête d'ajout de la Categorie
    var insertionCategorie = 'INSERT INTO Stoc_mv(idStock,idProduit,dateInventaire,quantite) VALUES (' + this.getIdStock() + ',' + this.getIdProduit() + ',\'' + this.getDate() + '\',' + this.getQuantite() + ')'
    db.query(insertionCategorie, function (err) {
      if (err) throw err
      else console.log('1 row inserted in Stoc_mv')
    })
  }

  // Getter & Setters
  getIdStock () {
    return this.idStock
  }

  getIdProduit () {
    return this.idProduit
  }

  getDate () {
    return this.date
  }

  getQuantite () {
    return this.quantite
  }

  setIdStock (idStock) {
    this.idStock = idStock
  }

  setIdProduit (idProd) {
    this.idProduit = idProd
  }

  setDate (date) {
    this.date = date
  }

  setQuantite (qt) {
    this.quantite = qt
  }
}

function JSDateToMYSQLDate (dateJs) {
  var SQLDate = dateJs.toISOString().split('T')[0] + ' ' + dateJs.toTimeString().split(' ')[0]
  return SQLDate
}
