const dataModelObject = require('./dataModelObject.js')

module.exports = class ProduitVente extends dataModelObject {
  // Constructeur
  constructor (idProduit, idVente, quantite) {
    super()
    if (idProduit === undefined || idVente === undefined) throw new Error('ID missing in ProduitVente') // Pas d'id, pas de construction
    if (isNaN(quantite) || quantite < 0) quantite = 0

    this.idProduit = idProduit
    this.idVente = idVente
    this.quantite = quantite
  }

  persist (db) {
    // Requête d'ajout de la Categorie
    var insertionCategorie = 'INSERT INTO ProduitVente(idProduit,idVente,quantite) VALUES (' + this.getIdProduit() + ',' + this.getIdVente() + ',' + this.getQuantite() + ')'
    db.query(insertionCategorie, function (err) {
      if (err) throw err
      else console.log('1 row inserted in ProduitVente')
    })
  }

  // Getter & Setters
  getIdPV () {
    return this.idPV
  }

  getIdProduit () {
    return this.idProduit
  }

  getIdVente () {
    return this.idVente
  }

  getQuantite () {
    return this.quantite
  }

  setIdPV (idPV) {
    this.idPV = idPV
  }

  setIdProduit (idProd) {
    this.idProduit = idProd
  }

  setIdVente (idVente) {
    this.idVente = idVente
  }

  setQuantite (qt) {
    this.quantite = qt
  }
}
