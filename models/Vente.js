const dataModelObject = require('./dataModelObject.js')

module.exports = class Vente extends dataModelObject {
  // Constructeur
  constructor (prixTotal, date) {
    super()
    if (date === undefined) date = JSDateToMYSQLDate(new Date())
    if (prixTotal === undefined || isNaN(prixTotal)) prixTotal = 1000

    this.date = date
    this.prixTotal = prixTotal
  }

  persist (db) {
    // Requête d'ajout de la Categorie
    var insertionCategorie = 'INSERT INTO Vente(dateVente,prixTotal) VALUES (\'' + this.getDate() + '\',' + this.getPrixTotal() + ')'
    db.query(insertionCategorie, function (err) {
      if (err) throw err
      else console.log('1 row inserted in Vente')
    })
  }

  // Getter & Setters
  getIdVente () {
    return this.idVente
  }

  getDate () {
    return this.date
  }

  getPrixTotal () {
    return this.prixTotal
  }

  setIdVente (idVente) {
    this.idVente = idVente
  }

  setDate (date) {
    this.date = date
  }

  setPrixTotal (prix) {
    this.prixTotal = prix
  }
}

function JSDateToMYSQLDate (dateJs) {
  var SQLDate = dateJs.toISOString().split('T')[0] + ' ' + dateJs.toTimeString().split(' ')[0]
  return SQLDate
}
