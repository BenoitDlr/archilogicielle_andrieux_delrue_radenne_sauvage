-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 04, 2020 at 03:44 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `node`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `idCat` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`idCat`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`idCat`, `label`, `description`) VALUES
(1, 'Inconnu', 'Categorie dont on ne sait rien'),
(2, 'Alimentaire', 'Ce qui se mange'),
(3, 'Chaussure_Magique', 'Moi je veux des chaussures');

-- --------------------------------------------------------

--
-- Table structure for table `prix`
--

DROP TABLE IF EXISTS `prix`;
CREATE TABLE IF NOT EXISTS `prix` (
  `idPrix` int(11) NOT NULL AUTO_INCREMENT,
  `idProduit` int(11) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `Valeur` float NOT NULL,
  PRIMARY KEY (`idPrix`,`idProduit`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prix`
--

INSERT INTO `prix` (`idPrix`, `idProduit`, `dateDebut`, `dateFin`, `Valeur`) VALUES
(41, 109, '2020-02-04', '2199-12-31', 28);

-- --------------------------------------------------------

--
-- Table structure for table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `idProduit` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  PRIMARY KEY (`idProduit`)
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produit`
--

INSERT INTO `produit` (`idProduit`, `nom`, `idCategorie`) VALUES
(109, 'Chaussure', 1);

-- --------------------------------------------------------

--
-- Table structure for table `produitvente`
--

DROP TABLE IF EXISTS `produitvente`;
CREATE TABLE IF NOT EXISTS `produitvente` (
  `idPV` int(11) NOT NULL AUTO_INCREMENT,
  `idProduit` int(11) NOT NULL,
  `idVente` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`idPV`,`idProduit`,`idVente`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produitvente`
--

INSERT INTO `produitvente` (`idPV`, `idProduit`, `idVente`, `quantite`) VALUES
(1, 109, 1, 10),
(2, 1, 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `stoc_mv`
--

DROP TABLE IF EXISTS `stoc_mv`;
CREATE TABLE IF NOT EXISTS `stoc_mv` (
  `idStock` int(11) NOT NULL,
  `idProduit` int(11) NOT NULL,
  `dateInventaire` datetime NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`idStock`,`idProduit`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stoc_mv`
--

INSERT INTO `stoc_mv` (`idStock`, `idProduit`, `dateInventaire`, `quantite`) VALUES
(1, 109, '2020-02-04 15:13:49', 20);

-- --------------------------------------------------------

--
-- Table structure for table `vente`
--

DROP TABLE IF EXISTS `vente`;
CREATE TABLE IF NOT EXISTS `vente` (
  `idVente` int(11) NOT NULL AUTO_INCREMENT,
  `dateVente` datetime NOT NULL,
  `prixTotal` float NOT NULL,
  PRIMARY KEY (`idVente`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vente`
--

INSERT INTO `vente` (`idVente`, `dateVente`, `prixTotal`) VALUES
(1, '2020-02-04 11:49:29', 1500),
(2, '2020-02-04 15:13:49', 1500);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
